const mongoose = require("mongoose");

const StrongPasswordCheck = new mongoose.Schema(
  {
    input: { type: String, index: true },
    output: { type: Number, index: true },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("StrongPasswordCheck", StrongPasswordCheck);
