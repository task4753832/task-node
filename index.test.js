const request = require("supertest");
const app = require("./index");

describe("API Endpoints", () => {
  it("check password strength ", async () => {
    const response = await request(app)
      .post("/api/password-strength")
      .send({ password: "a" });

    expect(response.status).toEqual(200);
    expect(response.body.result).toBe(5);
  });
  //   check second case
  it("check password strength ", async () => {
    const response = await request(app)
      .post("/api/password-strength")
      .send({ password: "aA1" });

    expect(response.status).toEqual(200);
    expect(response.body.result).toBe(3);
  });
  //   check third case
  it("check password strength ", async () => {
    const response = await request(app)
      .post("/api/password-strength")
      .send({ password: "1337C0d3" });

    expect(response.status).toEqual(200);
    expect(response.body.result).toBe(0);
  });
  //   check fourth case
  it("check password strength ", async () => {
    const response = await request(app)
      .post("/api/password-strength")
      .send({ password: "Vija54as" });

    expect(response.status).toEqual(200);
    expect(response.body.result).toBe(0);
  });
});
