// password-partition-backend/server.js
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const PasswordSchema = require("./model");

mongoose.connect("mongodb://127.0.0.1:27017/task", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.post("/api/password-strength", async (req, res) => {
  const { password } = req.body;

  function strongPasswordSteps(password) {
    const password_length =
      password.length > 6
        ? password.length < 20
          ? 0
          : password.length - 20
        : 6 - password.length;
    let count = 0;

    if (!/[a-z]/.test(password)) count++;
    if (!/[A-Z]/.test(password)) count++;
    if (!/[0-9]/.test(password)) count++;

    for (let i = 2; i < password.length; i++) {
      if (
        password[i] === password[i - 1] &&
        password[i - 1] === password[i - 2]
      ) {
        count++;
        i = i + 2;
      }
    }

    const total = count;

    return Math.max(password_length, total);
  }
  const result = strongPasswordSteps(password);

  const passwordSchema = new PasswordSchema({
    input: password,
    output: result,
  });
  await passwordSchema
    .save()
    .then(() => {
      console.log("data saved successfully");
    })
    .catch((err) => {
      console.log("error occurred ======>", err);
    });

  res.json({ result });
});

// app.post("/api/array-partitioning", (req, res) => {
//   const { number } = req.body;

//   const totalSum = number.reduce((sum, num) => sum + num, 0);

//   let checkPosable = new Array(totalSum + 1).fill(false);
//   checkPosable[0] = true;

//   for (let num of number) {
//     for (let j = totalSum; j >= num; j--) {
//         checkPosable[j] = checkPosable[j] || checkPosable[j - num];
//     }
//   }

//   let minDiff = totalSum;

//   for (let j = Math.floor(totalSum / 2); j >= 0; j--) {
//     if (checkPosable[j]) {
//       minDiff = totalSum - 2 * j;
//       break;
//     }
//   }
//   const minDifference = minDiff;
//   res.json({ minDifference });
// });

app.listen(3002, () => {
  console.log("Server is running on port 3002");
});

module.exports = app;
